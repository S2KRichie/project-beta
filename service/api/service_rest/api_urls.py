from django.urls import path
from service_rest.views import api_list_technicians, api_show_technicians, api_list_appointments, api_show_appointments, api_canceled_appointments, api_finished_appointments, api_get_automobileVOs

urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('technicians/<int:pk>', api_show_technicians, name="api_show_technicians"),
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('appointments/<int:pk>', api_show_appointments, name="api_show_appointments"),
    path('appointments/<int:pk>/canceled', api_canceled_appointments, name="api_canceled_appointment"),
    path('appointments/<int:pk>/finished', api_finished_appointments, name="api_finished_appointment"),
    path("automobileVOs/", api_get_automobileVOs, name="api_get_automobileVOs")
]
