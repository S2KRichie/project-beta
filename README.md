# CarCar

Team:

* Richard Kwong - Services
* Alex Mao - Sales

## Requirements

* Docker
* Git
* Node.js 18.2 or above

## Getting Started

1. Fork repository from https://gitlab.com/red878787/project-beta

2. Clone onto your local computer
```

git clone https://gitlab.com/red878787/project-beta
```

3. Build and run Docker volume and containers
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- Make sure all Docker containers are running

- Access through: http://localhost:3000

![Img](/images/CarCarWebsite.jpg)

## Design

CarCar is a car management system, comprising three interconnected microservices for inventory management, service scheduling, and sales operations. The system is designed to streamline the operations of car dealerships, service stations, and sales agencies

|    Microservice    |        Resource       |               Action              |
|:------------------:|:---------------------:|:---------------------------------:|
|      Inventory     |      Automobiles      |           Create Automobile       |
|                    |                       |         List of Automobiles       |
|                    |     Manufacturer      |          Create Manufacturer      |
|                    |                       |         List of Manufacturer      |
|                    |      Car Models       |            Create a Car Model     |
|                    |                       |            List Car Models        |
|       Service      |      Technician       |           Create a Technician     |
|                    |                       |         List of Technicians       |
|                    |       Services        |           Create Appointments     |
|                    |                       |         List of Appointments      |
|                    |                       |         List of Service History   |
|        Sales       |         Sales         |                Add a Sale         |
|                    |                       |            List of Sales          |
|                    |     Sales Person      |           add a Sales Person      |
|                    |                       |         List of Sales People      |
|                    |                       | List of Sales by each Sales Person|
|                    |       Customers       |              Add a Customer       |
|                    |                       |              Customer List        |

![Img](/images/CarCarDiagram.jpg)

## Inventory Microservice
The Inventory microservice is responsible for the management and organization of the automobile inventory within the CarCar dealership system. It deals with the creation, modification, and retrieval of data related to vehicles available for sale or already sold.



|  Inventory Models  |   Fields  |
|:------------------:|:---------:|
|    Manufacturer    |    name   |
|   VehicleModel     | name, picture URL, manufacturer |
|    Automobile      | color, year, VIN, sold, model |



1. Manufacturer: This model represents the automobile manufacturers. It contains a single field, name, which holds the name of the car manufacturer (like Toyota, BMW, Tesla, etc.).

2. VehicleModel: The VehicleModel model describes the specific models of cars produced by a manufacturer. It includes fields for name (representing the model's name, like Camry, Model S, X3, etc.), picture URL (a URL link to an image of the car model), and manufacturer. The VehicleModel is associated with one Manufacturer and one Manufacturer can produce many Vehicle Models.

3. Automobile: The Automobile  model represents individual cars in the inventory. It has fields for 'color', 'year', 'VIN' (Vehicle Identification Number) and 'sold'. The 'model' field is linking the Automobile to a VehivleModel and a VehicleModel can have many Automobiles.

The Inventory Microservice is crucial for the functionality of both the Sales and Services microservices, which rely on the Inventory microservice for details about the cars being sold or serviced.





## Service Microservice

The Services microservice manages the service-related aspects of the CarCar dealership. It handles scheduling, updating, and tracking of service appointments for vehicles sold.



| Services Models | Fields |
|:---------------:|:------:|
|  AutomobileVO   | VIN, sold |
|   Technician    | first name, last name, employee_id |
|  Appointment    | date_time, reason, status, VIN, customer, technician |



1. AutomobileVO: This model represents individual cars within the scope of the Services microservice. It has VIN (Vehicle Identification Number) and sold fields, providing necessary information about each automobile related to servicing.

2. Technician: This model represents the technicians who service the cars. It contains first name, last name, and employee_id fields. This model is necessary for tracking who serviced a particular car and managing the technicians' workload.

3. Appointment: This model represents the service appointments scheduled for each car. It has date_time, reason, status, VIN (the Vehicle Identification Number of the car to be serviced), and customer. It also includes a technician field, which is a foreign key linking to the Technician model. This relationship means that each Appointment is associated with a specific Technician, and a Technician can be associated with multiple Appointments.

The Services microservice provides comprehensive information about the service-related aspects of the dealership's operations. This includes keeping track of which automobiles have been or need to be serviced, who the technicians are, and details about each service appointment.

## Sales Microservice

The Sales microservice is for handling and tracking the car sales activities within the CarCar dealership system. It deals with managing the details of automobiles sold, salespersons, customers, and individual sales records.



|  Sales Models  |   Fields  |
|:--------------:|:---------:|
|  AutomobileVO  |   VIN, sold   |
|  SalesPerson   | first name, last name, employee_id |
|    Customer    | first name, last name, address, phone number |
|     Sale      | automobile, salesperson, customer |



1. AutomobileVO: This model represents individual cars in the sales context. It contains VIN (Vehicle Identification Number) and sold fields, which indicate whether the car has been sold.

2. SalesPerson: This model represents the employees who sell cars. It contains first name, last name, and employee_id fields. This model is necessary for tracking who sold each car, helping in performance evaluation and commission calculations.

3. Customer: This model represents the customers who purchase cars. It has fields for the first name, last name, address, and phone number. This data helps in customer relationship management and sales tracking.

4. Sale: This model represents a single sale transaction within the dealership. It includes automobile, salesperson, customer, and price fields. The automobile, salesperson, and customer fields are all foreign keys linking to the AutomobileVO, Salesperson, and Customer models respectively.

The Sales microservice is able to provide comprehensive information about the car sales activities within the dealership. It manages the relationships among the cars sold, salespersons, and customers, offering a detailed perspective of the sales operations.

## API Documentation


## Service Microservice
It's hosted on a 8080 port.


### TECHNICIANS
| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/


An example response for "List technicians":
```
{
    "technicians": [
        {
            "first_name": "Billy",
            "last_name": "W.",
            "id": 10,
            "employee_id": "415"
        },
        {
            "first_name": "Eric",
            "last_name": "L",
            "id": 11,
            "employee_id": "69"
        },
        {
            "first_name": "Lily",
            "last_name": "P.",
            "id": 12,
            "employee_id": "305"
        }
    ]
}
```


An example response for "Technician Detail":
Replace the "<int:pk>" with the corresponding "id" integer to retrieve data for that specific technician.
```
{
    "first_name": "Billy",
    "last_name": "W.",
    "employee_id": "415"
}
```
An example input/response for "Create a Technician":


INPUT:
```
{
    "first_name": "teddy",
    "last_name": "Wong",
    "employee_id": "5"
}
```
RESPONSE:
```
{
    "first_name": "teddy",
    "last_name": "Wong",
    "employee_id": "5"
}
```


An example response for "Delete a technician":
Replace the "<int:pk>" with the corresponding "id" integer to DELETE that specific technician.
```
{
    "Deleted": true
}
```


### APOINTMENTS
| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>
| Cancel appointment | POST | http://localhost:8080/api/appointments/<int:id>/canceled
| Finished service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>/finished


An example response for "List service appointments":
```
{
    "appointments": [
        {
            "date_time": "2023-06-09",
            "reason": "Oil",
            "status": "",
            "customer": "test1",
            "vin": "1234",
            "id": 5,
            "technician": {
                "first_name": "Billy",
                "last_name": "W.",
                "employee_id": "415"
            }
        },
        {
            "date_time": "2023-06-07",
            "reason": "Oil",
            "status": "Pending",
            "customer": "David",
            "vin": "123",
            "id": 9,
            "technician": {
                "first_name": "Billy",
                "last_name": "W.",
                "employee_id": "415"
            }
        }
    ]
}
```


An example response of "Service appointment Detail"
Replace the "<int:pk>" with the corresponding "id" integer to retrieve that specific appointment detail.
```
{
    "appointments": {
        "date_time": "2023-06-07",
        "reason": "Oil",
        "status": "Finished",
        "customer": "Lily",
        "vin": "123",
        "id": 1,
        "technician": {
            "first_name": "Billy",
            "last_name": "W.",
            "employee_id": "415"
        }
    }
}
```


Example input/response "Create service appointment":
INPUT
```
{
            "date_time": "2023-06-07",
            "reason": "Oil",
            "status": "Pending",
            "customer": "David",
            "vin": "123",
            "id": 9,
            "technician": 10
}
```
RESPONSE
```
[
    {
        "date_time": "2023-06-07T00:00:00",
        "reason": "Oil",
        "status": "Pending",
        "customer": "David",
        "vin": "123",
        "id": 9,
        "technician": {
            "first_name": "Billy",
            "last_name": "W.",
            "employee_id": "415"
        }
    },
    true
]
```


Example response for "Delete service appointment":
```
{
    "Deleted": true
}
```


Example response for "cancel service appointment":
```
{
    "date_time": "2023-06-09",
    "reason": "Oil",
    "status": "canceled",
    "customer": "test1",
    "vin": "1234",
    "id": 5,
    "technician": {
        "first_name": "Billy",
        "last_name": "W.",
        "employee_id": "415"
    }
}
```
Example response for "finished service appointment":
```
{
    "date_time": "2023-06-09",
    "reason": "Oil",
    "status": "Finished",
    "customer": "test1",
    "vin": "1234",
    "id": 5,
    "technician": {
        "first_name": "Billy",
        "last_name": "W.",
        "employee_id": "415"
    }
}
```
## Sales Microservice

The sales microservice is hosted at port 8090.

### Customers:


| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"name": "John Doe",
	"address": "123 Dream Drive",
	"phone_number": "1235556666"
}
```
Return Value of Creating a Customer:
```
{
	"id: "1",
	"name": "John Doe",
	"address": "123 Dream Drive",
	"phone_number": "1235556666"
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"id",
			"name": "Jane Doe",
			"address": "321 Go St.",
			"phone_number": "9998887777"
		},
		{
			"id",
			"name": "John Doe",
			"address": "123 Dream Drive",
			"phone_number": "1235556666"
		}
	]
}
```
### Salespeople:


| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson Sales details | GET | http://localhost:8090/api/salesperson/id/sales/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/

List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "John",
            "last_name": "Doe",
			"employee_id": "1"
		}
	]
}
```

Show a Salesperson's Sales History Return Value:
```
[
	{
		"id": 1,
		"price": 2000.0,
		"automobile": {
			"vin": "2C3CC5FN2AN234564",
			"sold": false
		},
		"salesperson": {
			"id": 1,
			"first_name": "Alex",
			"last_name": "mao",
			"employee_id": "1"
		},
		"customer": {
			"id": 1,
			"first_name": "asdf",
			"last_name": "asdf",
			"address": "123",
			"phone_number": "123-555-6666"
		}
	}
]
```

To create a salesperson (SEND THIS JSON BODY):
```
	{
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": "1"
	}
```

Return Value of creating a salesperson:
```
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "1"
}
```


### Sales:

- the id value to show a salesperson's sales history is the **"id" value tied to a salesperson.**
- the id value in delete a sale is the **"id" value tied to the sale.**

| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List all sales history | GET | http://localhost:8090/api/sales
| Create a new sale | POST | http://localhost:8090/api/sales
| Delete a sale | DELETE | http://localhost:8090/api/sales/id

List all Sales Return Value:
```
{
	"sales": [
		{
			"id": 1,
			"automobile": {
				"vin": "2C3CC5FN2AN234564",
				"sold": false
			},
			"salesperson": {
				"id": 1,
				"first_name": "Alex",
				"last_name": "Mao",
				"employee_id": "1"
			},
			"customer": {
				"first_name": "test_first",
				"last_name": "test_last",
				"address": "123 street st.",
				"phone_number": "123-555-6666",
				"id": 1
			},
			"price": 2000
		},
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
    "automobile": "5C3YC5FB2AN120188",
    "salesperson": "1",
    "customer": "1",
    "price": 25000.00
}
```
Return Value of Creating a New Sale:
```
{
	"id": 5,
	"automobile": {
		"vin": "5C3YC5FB2AN120188",
		"sold": false
	},
	"salesperson": {
		"id": 2,
		"first_name": "test_first",
		"last_name": "mao",
		"employee_id": "1"
	},
	"customer": {
		"first_name": "test_first",
		"last_name": "test_last",
		"address": "123 street st.",
		"phone_number": "123-555-6666",
		"id": 1
	},
	"price": "25000"
}
```

Delete a Sale: Send a DELETE request to the provided url to delete a sale from the database.

A Deleted Sale Return:
```
{
	"Deleted": true
}
```

## Inventory Microservice

### Manufacturers

| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

Example response for “List manufacturers”:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Honda"
		}
	]
}
```

Example of input/response “Create a manufacturer”:
```
{
  "name": "Toyota"
}
```
```
{
	"href": "/api/manufacturers/7/",
	"id": 7,
	"name": "Toyota"
}
```
Example response of “Get a specific manufacturer”:
http://localhost:8100/api/manufacturers/7
```
{
	"href": "/api/manufacturers/7/",
	"id": 7,
	"name": "Toyota"
}
```

Example input/response of “Update a specific manufacture”:
http://localhost:8100/api/manufacturers/7
```
{
	"href": "/api/manufacturers/7/",
	"id": 7,
	"name": "Bugatti"
}
```
```
{
	"href": "/api/manufacturers/7/",
	"id": 7,
	"name": "Bugatti"
}
```

Example response of “Delete a specific manufacturer”:
```
{
	"id": null,
	"name": "Bugatti"
}
```

### Vehicle Models

| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Example input/response of “create a vehicle model”:
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

```
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
Example response of “List vehicle model”:
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://hips.hearstapps.com/hmg-prod/images/c-005-1500x1000-1652713137.jpg?crop=0.891xw:1.00xh;0.0554xw,0&resize=640:*",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```
Example response of  “Get a specific manufacturer”:
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/images/c-005-1500x1000-1652713137.jpg?crop=0.891xw:1.00xh;0.0554xw,0&resize=640:*",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
Example input/response of  “Update a specific vehicle model”:
Input
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```
Response
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
Example response of “Delete vehicle model”:
```
{
	"id": null,
	"name": "S2000",
	"picture_url": "https://media.carsandbids.com/cdn-cgi/image/width=2080,quality=70/da6e8a1d4896127a9d2adf83461993b45339474c/photos/35ERebOR-WSJ1qJbGwR-(edit).jpg?t=167180669720",
	"manufacturer": {
		"href": "/api/manufacturers/6/",
		"id": 6,
		"name": "Honda"
	}
}
```
### Automobiles
| Action | Method | URL
| :-----------: | :-----------: | :-----------: |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Example input/response of “create an automobile”:
Input
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Response
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 2,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
Example response of “Get a specific automobile”:
Response
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 2,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```

Example response of “List automobile”:
```
{
	"autos": [
		{
			"href": "/api/automobiles/2C3CC5FN2AN234564/",
			"id": 1,
			"color": "Yellow",
			"year": 1987,
			"vin": "2C3CC5FN2AN234564",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": true
		},

	]
}
```
Example input/response of “Update a specific automobile”:

Input
```
{
  "color": "red",
  "year": 2012,
  "sold": true
}

```
Response
```
{
	"href": "/api/automobiles/2C3CC5FN2AN234564/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "2C3CC5FN2AN234564",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": true
}
```
Example response of “Delete a specific automobile”:
```
{
	"href": "/api/automobiles/2C3CC5FN2AN234564/",
	"id": null,
	"color": "red",
	"year": 2012,
	"vin": "2C3CC5FN2AN234564",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": true
}
```


### Value Objects
In this application, the primary value object is the AutomobileVO. It serves as a bridge between the Inventory microservice and the Sales and Services microservices. The purpose of the AutomobileVO model is to retrieve property data from the automobile models stored in the Inventory microservice's database and store it within the AutomobileVO model.